# README #

This repository contains the latest ZBOS MQTT API.
The ZBOS MQTT API spec is based on [AsyncAPI](https://www.asyncapi.com) and generated from our internal codebase.

### What is this repository for? ###

You can use the provided json files to generate documentation or code.

Some of the available code generators include:

- Java
- Python
- NodeJS

For more information see the AsyncAPI website.

### More info ###

* [Zorabots Documentation Portal](https://docs.zorabots.be)
* [Zorabots Support Portal](https://support.zorabots.be)
* [Zorabots website](https://www.zorabots.be)