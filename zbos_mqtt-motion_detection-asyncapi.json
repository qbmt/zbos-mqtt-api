{
  "asyncapi" : "2.0.0",
  "id" : "urn:zbos-mqtt-api",
  "defaultContentType" : "application/json",
  "info" : {
    "title" : "Motion detection",
    "version" : "2.12.1",
    "description" : "API for communication with ZBOS by Zora Robotics.",
    "contact" : {
      "email" : "info@zorarobotics.be"
    }
  },
  "channels" : {
    "zbos/security/motiondetect/start" : {
      "publish" : {
        "summary" : "Start motion detection",
        "description" : "",
        "tags" : [ {
          "name" : "Motion detection",
          "description" : "All motion detection related topics."
        } ],
        "message" : {
          "payload" : {
            "type" : "object",
            "properties" : {
              "key" : {
                "description" : "Required key",
                "type" : "string"
              },
              "unit" : {
                "description" : "Can be \"s\", \"h\" or \"m\" (seconds, hours, minutes), used for duration. Defaults to seconds",
                "type" : "string"
              },
              "initialDelay" : {
                "description" : "How long to wait until starting motion detection, in milliseconds. Defaults to 2000, lower values may cause false positives",
                "type" : "number",
                "minimum" : 0
              },
              "interval" : {
                "description" : "How often the camera should check for motion, in milliseconds. Defaults to 200",
                "type" : "number",
                "minimum" : 0
              },
              "cooldown" : {
                "description" : "How long to wait after motion is detected before a detection can be triggered again, in milliseconds. Defaults to 0",
                "type" : "number",
                "minimum" : 0
              },
              "duration" : {
                "description" : "The maximum time the motion detection will be active, after the time is elapsed, the motion detection will stop. Defaults to 10",
                "type" : "number",
                "minimum" : 0
              },
              "stopOnDetection" : {
                "description" : "If true the motion detection will stop after the first detection. Defaults to true",
                "type" : "boolean"
              },
              "generateMonitoringEvent" : {
                "description" : "If true, a monitoring event will be created, even if the service is disabled.",
                "type" : "boolean"
              },
              "stopAfterDuration" : {
                "description" : "If true the motion detection will stop at the end of the duration. Defaults to true",
                "type" : "boolean"
              }
            }
          },
          "name" : "MotionDetectionOptions",
          "examples" : [ {
            "payload" : {
              "key" : "test",
              "unit" : "h",
              "initialDelay" : 2500,
              "interval" : 250,
              "cooldown" : 5000,
              "duration" : 7,
              "stopOnDetection" : true,
              "stopAfterDuration" : true
            }
          } ]
        }
      }
    },
    "zbos/security/motiondetect/stop" : {
      "publish" : {
        "summary" : "Stop motion detection",
        "description" : "",
        "tags" : [ {
          "name" : "Motion detection",
          "description" : "All motion detection related topics."
        } ],
        "message" : {
          "$ref" : "#/components/messages/keyMessage"
        }
      }
    },
    "zbos/security/motiondetect/event/{key}" : {
      "subscribe" : {
        "summary" : "event: Motion detected",
        "description" : "",
        "tags" : [ {
          "name" : "Motion detection",
          "description" : "All motion detection related topics."
        } ],
        "message" : {
          "payload" : {
            "type" : "object",
            "properties" : {
              "image" : {
                "description" : "Image of the detected motion, only given if upload is enabled in motion detection options.",
                "type" : "string"
              },
              "options" : {
                "description" : "Options that were used to start the motion detection.",
                "type" : "object",
                "properties" : {
                  "key" : {
                    "description" : "Required key",
                    "type" : "string"
                  },
                  "unit" : {
                    "description" : "Can be \"s\", \"h\" or \"m\" (seconds, hours, minutes), used for duration. Defaults to seconds",
                    "type" : "string"
                  },
                  "initialDelay" : {
                    "description" : "How long to wait until starting motion detection, in milliseconds. Defaults to 2000, lower values may cause false positives",
                    "type" : "number",
                    "minimum" : 0
                  },
                  "interval" : {
                    "description" : "How often the camera should check for motion, in milliseconds. Defaults to 200",
                    "type" : "number",
                    "minimum" : 0
                  },
                  "cooldown" : {
                    "description" : "How long to wait after motion is detected before a detection can be triggered again, in milliseconds. Defaults to 0",
                    "type" : "number",
                    "minimum" : 0
                  },
                  "duration" : {
                    "description" : "The maximum time the motion detection will be active, after the time is elapsed, the motion detection will stop. Defaults to 10",
                    "type" : "number",
                    "minimum" : 0
                  },
                  "stopOnDetection" : {
                    "description" : "If true the motion detection will stop after the first detection. Defaults to true",
                    "type" : "boolean"
                  },
                  "generateMonitoringEvent" : {
                    "description" : "If true, a monitoring event will be created, even if the service is disabled.",
                    "type" : "boolean"
                  },
                  "stopAfterDuration" : {
                    "description" : "If true the motion detection will stop at the end of the duration. Defaults to true",
                    "type" : "boolean"
                  }
                }
              }
            }
          },
          "name" : "MotionDetectionEvent",
          "examples" : [ {
            "payload" : {
              "image" : "SomeBase64Image",
              "options" : {
                "key" : "test",
                "unit" : "h",
                "initialDelay" : 2500,
                "interval" : 250,
                "cooldown" : 5000,
                "duration" : 7,
                "stopOnDetection" : true,
                "stopAfterDuration" : true
              }
            }
          } ]
        }
      },
      "parameters" : {
        "key" : {
          "description" : "Request key to create a unique subscription topic",
          "schema" : {
            "type" : "string"
          }
        }
      }
    },
    "zbos/security/motiondetect/started/{key}" : {
      "subscribe" : {
        "summary" : "Motion detection started",
        "description" : "",
        "tags" : [ {
          "name" : "Motion detection",
          "description" : "All motion detection related topics."
        } ],
        "message" : {
          "$ref" : "#/components/messages/emptyMessage"
        }
      },
      "parameters" : {
        "key" : {
          "description" : "Request key to create a unique subscription topic",
          "schema" : {
            "type" : "string"
          }
        }
      }
    },
    "zbos/security/motiondetect/stopped/{key}" : {
      "subscribe" : {
        "summary" : "Motion detection stopped",
        "description" : "",
        "tags" : [ {
          "name" : "Motion detection",
          "description" : "All motion detection related topics."
        } ],
        "message" : {
          "$ref" : "#/components/messages/emptyMessage"
        }
      },
      "parameters" : {
        "key" : {
          "description" : "Request key to create a unique subscription topic",
          "schema" : {
            "type" : "string"
          }
        }
      }
    },
    "zbos/security/motiondetect/timeout/event/{key}" : {
      "subscribe" : {
        "summary" : "event: Motion detection timeout",
        "description" : "",
        "tags" : [ {
          "name" : "Motion detection",
          "description" : "All motion detection related topics."
        } ],
        "message" : {
          "$ref" : "#/components/messages/emptyMessage"
        }
      },
      "parameters" : {
        "key" : {
          "description" : "Request key to create a unique subscription topic",
          "schema" : {
            "type" : "string"
          }
        }
      }
    },
    "zbos/security/motiondetect/ready/get" : {
      "publish" : {
        "summary" : "Get motion detection ready",
        "description" : "see <<zbos/security/motiondetect/ready/get/response/{key}>> for response\n",
        "tags" : [ {
          "name" : "Motion detection",
          "description" : "All motion detection related topics."
        } ],
        "message" : {
          "$ref" : "#/components/messages/keyMessage"
        }
      }
    },
    "zbos/security/motiondetect/ready/get/response/{key}" : {
      "subscribe" : {
        "summary" : "response: Motion detection ready",
        "description" : "",
        "tags" : [ {
          "name" : "Motion detection",
          "description" : "All motion detection related topics."
        } ],
        "message" : {
          "$ref" : "#/components/messages/emptyMessage"
        }
      },
      "parameters" : {
        "key" : {
          "description" : "Request key to create a unique subscription topic",
          "schema" : {
            "type" : "string"
          }
        }
      }
    },
    "zbos/security/motiondetect/ready/event" : {
      "subscribe" : {
        "summary" : "event: Ready to start motion detection",
        "description" : "",
        "tags" : [ {
          "name" : "Motion detection",
          "description" : "All motion detection related topics."
        } ],
        "message" : {
          "$ref" : "#/components/messages/emptyMessage"
        }
      }
    }
  },
  "components" : {
    "schemas" : {
      "percentage" : {
        "description" : "Percentage value between with range 0 to 100",
        "type" : "integer",
        "maximum" : 100,
        "minimum" : 0
      },
      "key" : {
        "description" : "Required random key",
        "type" : "string"
      }
    },
    "messages" : {
      "emptyMessage" : {
        "name" : "EmptyMessage",
        "summary" : "Empty message"
      },
      "keyMessage" : {
        "payload" : {
          "type" : "object",
          "properties" : {
            "key" : {
              "description" : "Required random key",
              "type" : "string"
            }
          }
        },
        "name" : "KeyResult",
        "summary" : "Random key",
        "examples" : [ {
          "payload" : {
            "key" : "ABCxyz"
          }
        } ]
      },
      "successMessage" : {
        "payload" : {
          "type" : "object",
          "properties" : {
            "success" : {
              "type" : "boolean"
            },
            "message" : {
              "description" : "Optional error message",
              "type" : "string"
            }
          }
        },
        "name" : "SuccessMessage",
        "summary" : "Success message",
        "examples" : [ {
          "payload" : {
            "success" : true
          }
        } ]
      },
      "notificationMessage" : {
        "payload" : {
          "type" : "object",
          "properties" : {
            "message" : {
              "type" : "object",
              "properties" : {
                "message" : {
                  "type" : "string"
                },
                "translate" : {
                  "type" : "boolean"
                },
                "formatArguments" : {
                  "type" : "array",
                  "items" : {
                    "type" : "string"
                  }
                },
                "translationCategory" : {
                  "type" : "string"
                }
              }
            }
          }
        },
        "name" : "NotificationOptions",
        "summary" : "Message json",
        "examples" : [ {
          "payload" : {
            "message" : {
              "message" : "string",
              "translate" : true,
              "formatArguments" : [ "string" ]
            }
          }
        } ]
      }
    }
  },
  "tags" : [ {
    "name" : "Speech",
    "description" : "All speech related topics."
  }, {
    "name" : "Motion",
    "description" : "All motion related topics."
  }, {
    "name" : "Audio",
    "description" : "All audio related topics."
  }, {
    "name" : "Status",
    "description" : "All status related topics."
  }, {
    "name" : "System",
    "description" : "All system related topics."
  }, {
    "name" : "Sensors",
    "description" : "All sensors related topics."
  }, {
    "name" : "Leds",
    "description" : "All leds related topics."
  }, {
    "name" : "Composer",
    "description" : "All composer related topics."
  }, {
    "name" : "Kiosk",
    "description" : "All kiosk related topics."
  }, {
    "name" : "Media",
    "description" : "All media related topics."
  }, {
    "name" : "Applications",
    "description" : "All applications related topics."
  }, {
    "name" : "Translations",
    "description" : "All translations related topics."
  }, {
    "name" : "Settings",
    "description" : "All settings related topics."
  }, {
    "name" : "Camera",
    "description" : "All camera related topics."
  }, {
    "name" : "Domotics",
    "description" : "All domotics related topics."
  }, {
    "name" : "Cloud",
    "description" : "All cloud related topics."
  }, {
    "name" : "SLAM",
    "description" : "All slam related topics."
  }, {
    "name" : "Wifi",
    "description" : "All wifi related topics."
  }, {
    "name" : "Connection",
    "description" : "All connection related topics."
  }, {
    "name" : "Survey",
    "description" : "All survey related topics."
  }, {
    "name" : "Motion detection",
    "description" : "All motion detection related topics."
  }, {
    "name" : "Face tracking",
    "description" : "All face tracking related topics."
  }, {
    "name" : "SIP",
    "description" : "All sip related topics."
  }, {
    "name" : "Time",
    "description" : "All time related topics."
  }, {
    "name" : "Variables",
    "description" : "All variables related topics."
  }, {
    "name" : "Diagnostics",
    "description" : "All diagnostics related topics."
  } ]
}